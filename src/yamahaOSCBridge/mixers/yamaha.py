"""Classes representing different mixer boards

Module: yamaha
Author: Christoph Jabs
Project: YamahaOSCBridge
"""

# External imports
import mido
import time
import logging
# Internal imports
from . import basics

logger = logging.getLogger(__name__)


def find_output_port(input_port, mixer_type, device_id):
    """Finds the corresponding output port

    Checks which output port is connected to the mixer on the input port
    Returns the name of the output port connected to the given mixer"""
    for output_port_name in mido.get_output_names():
        with mido.open_output(output_port_name) as output_port:
            # Build message to request main mute state
            msg = mido.Message('sysex', data=[mixer_type._vendor_id,
                                              device_id - 1 + 0x30,
                                              mixer_type._device_type,
                                              mixer_type._general_parameter_id,
                                              0x01, 0x4D, 0x00, 0x00])
            output_port.send(msg)
            # Wait for answer
            time.sleep(0.1)
            expected_answer = (0x43, 0x10, 0x3E, 0x7F, 0x01, 0x4D, 0x00, 0x00)
            for msg in input_port.iter_pending():
                if msg.type == 'sysex':
                    if len(msg.data) == 12:
                        if msg.data[0:8] == expected_answer:
                            return output_port_name

def check_for_connected_mixers(midi_port):
    """Check if a mxier is connected at the given midi_port

    Returns a set of tuples (<class of mixer>, <id_number>)
    """
    logger.info('Checking for connected mixers on port {port}'
                .format(port=midi_port.name))
    mixer_list = [Y01v96]
    connected_mixers = set()
    # Wait for a few messages to arrive
    time.sleep(0.5)
    for msg in midi_port.iter_pending():
        if msg.type == 'sysex':
            for mixer in mixer_list:
                if (msg.data[0],) + msg.data[2:5] \
                        == (mixer._vendor_id, mixer._device_type,
                            mixer._specific_parameter_id,
                            mixer._general_parameter_id):
                    connected_mixers.add((mixer, msg.data[1] - 0x0F))
    return connected_mixers


class YamahaMixer(basics.Mixer):
    """Abstract base class for different mixer boards by Yamaha

    Includes basic functionality and properties
    """
    type_string = 'General Yamaha Mixer'
    _vendor_id = 0x43
    _device_type = None
    _general_parameter_id = 0x7F
    # Parameters to be set by inheriting classes
    _specific_parameter_id = None
    _channel_count, _aux_count, _bus_count = 0, 0, 0
    # Time to wait after each request
    time_per_request = 0.005

    def __init__(self, input_port_name, output_port_name, device_id):
        logger.info('Creating Yamaha mixer of type {type} and with ID '
                    '{id_number} on port {port}'.format(type=self.type_string,
                                                        id_number=device_id,
                                                        port=(input_port_name,
                                                              output_port_name)))
        self._device_id = device_id - 1
        input_port = mido.open_input(
            input_port_name, callback=self._midi_callback)
        output_port = mido.open_output(output_port_name)
        self._midi_port = mido.ports.IOPort(input_port, output_port)
        super().__init__()

    def _set_general_parameter(self, data_bytes):
        """Set parameter via SysEx, that applies to all Yamaha mixers

        The data bytes will be wrapped with the appropriate header and footer
        data_bytes: must be a list of bytes
        """
        # Generate MIDI message
        msg = mido.Message('sysex', data=[self._vendor_id, self._device_id + 0x10,
                                          self._device_type,
                                          self._general_parameter_id]
                           + data_bytes)
        # Transmit message
        self._midi_port.send(msg)

    def _set_specific_parameter(self, data_bytes):
        """Send a SysEx command that is modified for each mixer

        The data bytes will be wrapped with the appropriate header and footer
        data_bytes: must be a list of bytes
        """
        # Generate MIDI message
        msg = mido.Message('sysex', data=[self._vendor_id, self._device_id + 0x10,
                                          self._device_type,
                                          self._specific_parameter_id]
                           + data_bytes)
        # Transmit message
        self._midi_port.send(msg)

    def _request_general_parameter(self, data_bytes):
        """Request parameter via SysEx, that applies to all Yamaha mixers

        The data bytes will be wrapped with the appropriate header ander footer
        data_bytes: must be a list of bytes
        """
        # Generate MIDI message
        msg = mido.Message('sysex', data=[self._vendor_id, self._device_id + 0x30,
                                          self._device_type,
                                          self._general_parameter_id]
                           + data_bytes)
        # Transmit message
        self._midi_port.send(msg)
        # Wait for mixer to answer
        time.sleep(self.time_per_request)

    def _request_specific_parameter(self, data_bytes):
        """Abstract method for requesting parameter via SysEx, specific to one moxer board

        data_bytes must be a list of bytes
        """
        # Generate MIDI message
        msg = mido.Message('sysex', data=[self._vendor_id, self._device_id + 0x30,
                                          self._device_type,
                                          self._specific_parameter_id]
                           + data_bytes)
        # Transmit message
        self._midi_port.send(msg)
        # Assume about 4ms per request to not overflow buffer
        time.sleep(self.time_per_request)

    @staticmethod
    def _value_2_value_bytes(value):
        """Converts the value with bit_resolution to a given number of value_bytes

        value: must be a value from -134217728..134217727
        """
        # Check if value is valid
        if value < -134217728 or value > 134217727:
            raise ValueError(
                'value must be a value from -134217728..134217727')
        # Calculate value_bytes
        value_bytes = []
        for i in range(0, 4):
            value_bytes.insert(0, (int(value / 2**(7 * i))) % 2**7)
        return value_bytes

    @staticmethod
    def _value_bytes_2_value(value_bytes):
        """Converts the given value bytes to a integer value

        value_bytes: a list of 4 integers
        """
        # Check if value_bytes are valid
        for byte in value_bytes:
            if byte > 0x7F:
                raise ValueError("value_byte can't be bigger than 0x7F")
        # Check number of value_bytes
        if len(value_bytes) > 4:
            raise ValueError("Can't have more than 4 value_bytes")
        value = 0
        # Invert list
        value_bytes = value_bytes[::-1]
        for i in range(len(value_bytes)):
            value += value_bytes[i] * 2**(7 * i)
        # 2 complement
        if value > (2**27 - 1):
            value -= 2 * 2**27
        return value

    def _midi_callback(self, message):
        """Callback method for incomming MIDI messages"""
        status_message = mido.Message('sysex', data=[self._vendor_id,
                                                     self._device_id + 0x10,
                                                     self._device_type,
                                                     self._specific_parameter_id,
                                                     self._general_parameter_id])
        # Ignore status message or non SysEx messages
        if message == status_message or message.type != 'sysex':
            return
        # Ignore messages not from this mixer
        if message.data[:3] != (self._vendor_id, self._device_id + 0x10,
                                self._device_type):
            return
        logger.debug('Recieved SysEx message for mixer: {data}'
                     .format(data=message.data))
        if message.data[3] == self._specific_parameter_id:
            return self._specific_midi_callback(message)
        value = YamahaMixer._value_bytes_2_value(message.data[8:12])
        id_bytes = message.data[4:6]
        id_bytes += (message.data[6] % 3,)
        send_byte = message.data[6]
        channel_byte = message.data[7]
        if id_bytes[1] == 0x23:
            aux_number = int(send_byte / 3) + 1
        else:
            aux_number = channel_byte + 1
        try:
            self._general_command_map[id_bytes](self,
                                                channel_number=channel_byte + 1,
                                                aux_number=aux_number,
                                                bus_number=channel_byte + 1,
                                                value=value / (2**10 - 1))
        except KeyError:
            logger.info('Unknown MIDI message')

    def _specific_midi_callback(self, message):
        """Abstract method for MIDI callback specific to one mixer board

        To be implemented by inheriting class
        """
        pass

    def activate_channel(self, channel_number, value=True, **kwargs):
        """Activates the channel given by channel_number"""
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().activate_channel(channel_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Send data
        self._set_general_parameter([0x01, 0x1A, 0x00, channel_number - 1]
                                    + value_bytes)

    def set_channel_stereo(self, channel_number, value=True, **kwargs):
        """Set a channel stereo/mono (pairing it to a partner channel)"""
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().set_channel_stereo(channel_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Determine neighbor channel
        second_channel_number = self._channels.index(
            self._channels[channel_number - 1].stereo_partner) + 1
        # Send data
        self._set_general_parameter([0x01, 0x18, 0x00, channel_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x18, 0x00, second_channel_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x1E, 0x00, channel_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x1E, 0x00, second_channel_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x1F, 0x00, channel_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x1F, 0x00, second_channel_number - 1]
                                    + value_bytes)

    def fade_channel(self, channel_number, value, **kwargs):
        """Fades the channel with channel number to a given value

        value: must be a value from 0..1
        """
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Check if value is valid
        if value < 0 or value > 1:
            raise ValueError('value must be a value from 0..1')
        # Modify model
        super().fade_channel(channel_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(round((2**10 - 1)
                                                             * value))
        # Send data
        self._set_general_parameter([0x01, 0x1C, 0x00, channel_number - 1]
                                    + value_bytes)

    def fade_channel_aux(self, channel_number, aux_number, value, **kwargs):
        """Fades the channel with channel_number on the given aux out to a given value

        value: must be a value from 0..1
        """
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Check if value is valid
        if value < 0 or value > 1:
            raise ValueError('value must be a value from 0..1')
        # Check if aux_number exists on mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid aux_number')
        # Modify model
        super().fade_channel_aux(channel_number, aux_number, value)
        # Determine value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(round((2**10 - 1)
                                                             * value))
        # Send data
        self._set_general_parameter([0x01, 0x23, aux_number * 3 - 1,
                                     channel_number - 1] + value_bytes)

    def pre_post_channel_aux(self, channel_number, aux_number, value, **kwargs):
        """Set pre/post status of channel aux send

        value: a boolean (True: pre, False: post)
        """
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Check if aux_number exists on mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid aux_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().pre_post_channel_aux(channel_number, aux_number, value)
        # Determine value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Send data
        self._set_general_parameter([0x01, 0x23, aux_number * 3 - 2,
                                     channel_number - 1] + value_bytes)

    def activate_aux(self, aux_number, value, **kwargs):
        """Activates the aux given by aux_number

        value: a boolean
        """
        # Check if aux_number exists on mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid aux_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().activate_aux(aux_number, value)
        # Determine Value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Send data
        self._set_general_parameter([0x01, 0x36, 0x00, aux_number - 1]
                                    + value_bytes)

    def set_aux_stereo(self, aux_number, value=True, **kwargs):
        """Set a aux stereo/mono (pairing it to a partner aux)"""
        #  Check if aux_number exists for mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid aux_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().set_aux_stereo(aux_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Determine neighbor aux
        second_aux_number = self._auxes.index(
            self._auxes[aux_number - 1].stereo_partner) + 1
        # Send data
        self._set_general_parameter([0x01, 0x34, 0x00, aux_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x34, 0x00, second_aux_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x3B, 0x00, aux_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x3B, 0x00, second_aux_number - 1]
                                    + value_bytes)

    def fade_aux(self, aux_number, value, **kwargs):
        """Fades the aux given by aux_number to value

        value: must be a value from 0..1
        """
        # Check if aux_number exists on mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid aux_number')
        # Check if value is valid
        if value < 0 or value > 1:
            raise ValueError('value must be a value from 0..1')
        # Modify model
        super().fade_aux(aux_number, value)
        # Determine value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(round((2**10 - 1)
                                                             * value))
        # Send data
        self._set_general_parameter([0x01, 0x39, 0x00, aux_number - 1]
                                    + value_bytes)

    def activate_bus(self, bus_number, value, **kwargs):
        """Activates the bus given by bus_number

        value: a boolean
        """
        # Check if bus_number exists on mixer
        if bus_number > self._bus_count or bus_number <= 0:
            raise ValueError('invalid bus_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().activate_bus(bus_number, value)
        # Determine Value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Send data
        self._set_general_parameter([0x01, 0x29, 0x00, bus_number - 1]
                                    + value_bytes)

    def set_bus_stereo(self, bus_number, value=True, **kwargs):
        """Set a bus stereo/mono (pairing it to a partner bus)"""
        #  Check if bus_number exists for mixer
        if bus_number > self._bus_count or bus_number <= 0:
            raise ValueError('invalid bus_number')
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().set_bus_stereo(bus_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Determine neighbor aux
        second_bus_number = self._buses.index(
            self._buses[bus_number - 1].stereo_partner) + 1
        # Send data
        self._set_general_parameter([0x01, 0x27, 0x00, bus_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x27, 0x00, second_bus_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x2D, 0x00, bus_number - 1]
                                    + value_bytes)
        self._set_general_parameter([0x01, 0x2D, 0x00, second_bus_number - 1]
                                    + value_bytes)

    def fade_bus(self, bus_number, value, **kwargs):
        """Fades the bus given by bus_number to value

        value: must be a value from 0..1
        """
        # Check if bus_number exists on mixer
        if bus_number > self._bus_count or bus_number <= 0:
            raise ValueError('invalid aux_number')
        # Check if value is valid
        if value < 0 or value > 1:
            raise ValueError('value must be a value from 0..1')
        # Modify model
        super().fade_bus(bus_number, value)
        # Determine value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(round((2**10 - 1)
                                                             * value))
        # Send data
        self._set_general_parameter([0x01, 0x2B, 0x00, bus_number - 1]
                                    + value_bytes)

    def activate_main(self, value, **kwargs):
        """Activates the main stereo bus"""
        # Cast value to bool
        value = bool(value)
        # Modify model
        super().activate_main(value)
        # Determine value bytes
        value_bytes = YamahaMixer._value_2_value_bytes(int(value))
        # Send data
        self._set_general_parameter([0x01, 0x4D, 0x00, 0x00] + value_bytes)
        self._set_general_parameter([0x01, 0x4D, 0x00, 0x01] + value_bytes)

    def fade_main(self, value, **kwargs):
        """Fades the main stereo bus to value

        value: must be a value from 0..1
        """
        # Check if value is valid
        if value < 0 or value > 1:
            raise ValueError('value must be a value from 0..1')
        # Modify model
        super().fade_main(value)
        # Determine value byter
        value_bytes = YamahaMixer._value_2_value_bytes(round((2**10 - 1)
                                                             * value))
        # Send data
        self._set_general_parameter([0x01, 0x4F, 0x00, 0x00] + value_bytes)
        self._set_general_parameter([0x01, 0x4F, 0x00, 0x01] + value_bytes)

    def request_channel_data(self, channel_number):
        """Request all data belonging to a channel"""
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Active status
        self._request_general_parameter([0x01, 0x1A, 0x00, channel_number - 1])
        # Level
        self._request_general_parameter([0x01, 0x1C, 0x00, channel_number - 1])
        # Stereo
        self._request_general_parameter([0x01, 0x18, 0x00, channel_number - 1])
        # Aux sends
        for i in range(self._aux_count):
            self._request_general_parameter([0x01, 0x23, (i + 1) * 3 - 1,
                                             channel_number - 1])
            self._request_general_parameter([0x01, 0x23, (i + 1) * 3 - 2,
                                             channel_number - 1])

    def request_main_data(self):
        """Request all data belonging to main"""
        # Active status
        self._request_general_parameter([0x01, 0x4D, 0x00, 0x00])
        # Level
        self._request_general_parameter([0x01, 0x4F, 0x00, 0x00])

    def request_aux_data(self, aux_number):
        """Request all data belonging to a aux"""
        # Active status
        self._request_general_parameter([0x01, 0x36, 0x00, aux_number - 1])
        # Level
        self._request_general_parameter([0x01, 0x39, 0x00, aux_number - 1])
        # Stereo
        self._request_general_parameter([0x01, 0x34, 0x00, aux_number - 1])

    def request_bus_data(self, bus_number):
        """Request all data belonging to a bus"""
        # Active status
        self._request_general_parameter([0x01, 0x29, 0x00, bus_number - 1])
        # Level
        self._request_general_parameter([0x01, 0x2B, 0x00, bus_number - 1])
        # Stereo
        self._request_general_parameter([0x01, 0x27, 0x00, bus_number - 1])

    _general_command_map = {(0x01, 0x1A, 0): basics.Mixer.activate_channel,
                            (0x01, 0x18, 0): basics.Mixer.set_channel_stereo,
                            (0x01, 0x1E, 0): basics.Mixer.set_channel_stereo,
                            (0x01, 0x1F, 0): basics.Mixer.set_channel_stereo,
                            (0x01, 0x1C, 0): basics.Mixer.fade_channel,
                            (0x01, 0x23, 2): basics.Mixer.fade_channel_aux,
                            (0x01, 0x23, 1): basics.Mixer.pre_post_channel_aux,
                            (0x01, 0x4D, 0): basics.Mixer.activate_main,
                            (0x01, 0x4F, 0): basics.Mixer.fade_main,
                            (0x01, 0x36, 0): basics.Mixer.activate_aux,
                            (0x01, 0x34, 0): basics.Mixer.set_aux_stereo,
                            (0x01, 0x3B, 2): basics.Mixer.set_aux_stereo,
                            (0x01, 0x39, 0): basics.Mixer.fade_aux,
                            (0x01, 0x29, 0): basics.Mixer.activate_bus,
                            (0x01, 0x27, 0): basics.Mixer.set_bus_stereo,
                            (0x01, 0x2D, 2): basics.Mixer.set_bus_stereo,
                            (0x01, 0x2B, 0): basics.Mixer.fade_bus}


class Y01v96(YamahaMixer):
    """Class for controlling the Yamaha 01v96 mixing board"""
    type_string = 'Yamaha 01v96'
    _device_type = 0x3E
    _specific_parameter_id = 0x0D
    _channel_count = 40
    _aux_count = 8
    _bus_count = 8

    def __init__(self, input_port_name, output_port_name, device_id):
        super().__init__(input_port_name, output_port_name, device_id)

    def _specific_midi_callback(self, message):
        """Method for handling MIDI messages specific to this mixer board"""
        value = YamahaMixer._value_bytes_2_value(message.data[8:12])
        id_bytes = message.data[4:6]
        channel_byte = message.data[7]
        char_byte = message.data[6]
        # Treat short names as first characters of names
        if char_byte < 4:
            char_byte += 4
        # STs
        if id_bytes == (0x02, 0x17):
            channel_byte = channel_byte * 2 + 32
        try:
            self._specific_command_map[id_bytes](self,
                                                 channel_number=channel_byte + 1,
                                                 aux_number=channel_byte + 1,
                                                 character_number=char_byte - 3,
                                                 value=value)
            if id_bytes == (0x02, 0x17):
                # Second channel of ST
                channel_byte += 1
                self._specific_command_map[id_bytes](self,
                                                     channel_number=channel_byte + 1,
                                                     aux_number=channel_byte + 1,
                                                     character_number=char_byte - 3,
                                                     value=value)
        except KeyError:
            logger.info('Unknwon MIDI message')

    def set_channel_name_char(self, channel_number, character_number, value,
                              **kwargs):
        """Set a character of a channels name

        value: the ASCII value of the character
        """
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Check if character_number is valid
        if character_number > self._name_length or character_number < 1:
            raise ValueError('invalid character_number')
        # Check if value is valid
        if value < 0 or value > 127:
            raise ValueError('value must be a value from 0..127')
        # Modify model
        super().set_channel_name_char(channel_number, character_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(value)
        # If ST
        if channel_number > 32:
            channel_number = int((channel_number - 33) / 2)
            # Send data
            self._set_specific_parameter([0x02, 0x17, character_number + 3,
                                          channel_number - 1] + value_bytes)
        else:
            # Send data
            self._set_specific_parameter([0x02, 0x04, character_number + 3,
                                          channel_number] + value_bytes)

    def set_aux_name_char(self, aux_number, character_number, value, **kwargs):
        """Set a character of an auxes name

        value: the ASCII value of the character
        """
        #  Check if aux_number exists for mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid channel_number')
        # Check if character_number is valid
        if character_number > self._name_length or character_number < 1:
            raise ValueError('invalid character_number')
        # Check if value is valid
        if value < 0 or value > 127:
            raise ValueError('value must be a value from 0..127')
        # Modify model
        super().set_aux_name_char(aux_number, character_number, value)
        # Determine value_bytes
        value_bytes = YamahaMixer._value_2_value_bytes(value)
        # Send data
        self._set_specific_parameter([0x02, 0x10, character_number + 3,
                                      aux_number - 1] + value_bytes)

    def request_channel_data(self, channel_number):
        """Request all data belonging to a channel"""
        #  Check if channel_number exists for mixer
        if channel_number > self._channel_count or channel_number <= 0:
            raise ValueError('invalid channel_number')
        # Channel name
        for char_number in range(16):
            if channel_number < 33:
                self._request_specific_parameter([0x02, 0x04, char_number + 4,
                                                  channel_number - 1])
            else:
                st_number = int((channel_number - 33) / 2)
                self._request_specific_parameter([0x02, 0x17, char_number + 4,
                                                  st_number])
        super().request_channel_data(channel_number)

    def request_aux_data(self, aux_number):
        """Request all data belonging to an aux"""
        #  Check if channel_number exists for mixer
        if aux_number > self._aux_count or aux_number <= 0:
            raise ValueError('invalid channel_number')
        # Channel name
        for char_number in range(16):
            self._request_specific_parameter([0x02, 0x10, char_number + 4,
                                              aux_number - 1])
        super().request_aux_data(aux_number)

    _specific_command_map = {(0x02, 0x04): basics.Mixer.set_channel_name_char,
                             (0x02, 0x17): basics.Mixer.set_channel_name_char,
                             (0x02, 0x10): basics.Mixer.set_aux_name_char}
