"""Classes representing parts of a mixer board

Module: parts
Author: Christoph Jabs
Project: YamahaOSCBridge
"""

# External imports
import logging

logger = logging.getLogger(__name__)


class Mixer:
    """Abstract base class for mixers

    This class holds the data model of the state the mixer is in
    Clients can subscribe to this class to recieve updates
    """
    type_string = 'Generic Mixer'

    # Properties to be set for inheriting classes
    _channel_count, _aux_count, _bus_count = 0, 0, 0

    def __init__(self):
        self._auxes = [Aux() for i in range(self._aux_count)]
        self._buses = [Bus() for i in range(self._bus_count)]
        self._channels = [Channel(self._auxes)
                          for i in range(self._channel_count)]
        self._main = Channel([])
        self._subscribers = []
        self.synchronize()

    def activate_channel(self, channel_number, value, **kwargs):
        """Activate or deactivate a channel"""
        logger.debug('Activate Channel {channel} to {value}'
                     .format(channel=channel_number, value=value))
        channel = self._channels[channel_number - 1]
        # Cast value to bool
        value = bool(value)
        channel.active = value
        self._notify_subscribers('/channel/' + str(channel_number) + '/mute')
        if channel.stereo_partner:
            # If stereo, also activate second channel
            channel.stereo_partner.active = value
            self._notify_subscribers(
                '/channel/' + str(self._channels.index(channel.stereo_partner) +
                                  1) + '/mute')

    def set_channel_stereo(self, channel_number, value, **kwargs):
        """Switch a channel between stereo and mono"""
        # Cast value to bool
        value = bool(value)
        # Get second_channel_number
        second_channel_number = channel_number + \
            (1 if channel_number % 2 else -1)
        logger.debug('Set channels {channel} and {channel_2} stereo {value}'
                     .format(channel=channel_number,
                             channel_2=second_channel_number, value=value))
        self._channels[channel_number - 1].stereo_partner = \
            self._channels[second_channel_number - 1] if value else None
        self._channels[second_channel_number - 1].stereo_partner = \
            self._channels[channel_number - 1] if value else None
        self._notify_subscribers('/channel/' + str(channel_number) + '/stereo')
        self._notify_subscribers(
            '/channel/' + str(second_channel_number) + '/stereo')

    def fade_channel(self, channel_number, value, **kwargs):
        """Change the level of a channel"""
        logger.debug('Fade Channel {channel} to {value}'
                     .format(channel=channel_number, value=value))
        channel = self._channels[channel_number - 1]
        channel.level = value
        self._notify_subscribers('/channel/' + str(channel_number) + '/level')
        if channel.stereo_partner:
            # If stereo, also fade second channel
            channel.stereo_partner.level = value
            self._notify_subscribers(
                '/channel/' + str(self._channels.index(channel.stereo_partner) +
                                  1) + '/level')

    def fade_channel_aux(self, channel_number, aux_number, value, **kwargs):
        """Change the send level of a channel to a aux"""
        logger.debug('Fade Channel {channel} on Aux {aux} to {value}'
                     .format(channel=channel_number, aux=aux_number, value=value))
        channel = self._channels[channel_number - 1]
        aux = self._auxes[aux_number - 1]
        channel.aux_sends[aux].level = value
        self._notify_subscribers('/channel/' + str(channel_number) + '/send/'
                                 + str(aux_number) + '/level')
        if aux.stereo_partner:
            # If stereo aux, also fade second send
            channel.aux_sends[aux.stereo_partner].level = value
            self._notify_subscribers('/channel/' + str(channel_number) +
                                     '/send/' +
                                     str(self._auxes.index(aux.stereo_partner) +
                                         1) + '/level')
        if channel.stereo_partner:
            # If stereo, also fade second channels send
            channel.stereo_partner.aux_sends[aux].level = value
            self._notify_subscribers(
                '/channel/' + str(self._channels.index(channel.stereo_partner) +
                                  1) + '/send/' + str(aux_number) + '/level')
            if aux.stereo_partner:
                # If stereo aux, also fade second send
                channel.stereo_partner.aux_sends[aux.stereo_partner].level = value
                self._notify_subscribers('/channel/' +
                                         str(self._channels.index(
                                             channel.stereo_partner) + 1) +
                                         '/send/' +
                                         str(self._auxes.index(
                                             aux.stereo_partner) + 1) +
                                         '/level')

    def pre_post_channel_aux(self, channel_number, aux_number, value, **kwargs):
        """Change a channel send between pre/post"""
        logger.debug('Set Channel {channel} Sends pre/post on Aux {aux} to {value}'
                     .format(channel=channel_number, aux=aux_number, value=value))
        channel = self._channels[channel_number - 1]
        aux = self._auxes[aux_number - 1]
        # Cast value to bool
        value = bool(value)
        channel.aux_sends[aux].pre_post = value
        self._notify_subscribers('/channel/' + str(channel_number) + '/send/'
                                 + str(aux_number) + '/pre_post')
        if aux.stereo_partner:
            # If stereo aux, also switch second auxs send
            channel.aux_sends[aux.stereo_partner].pre_post = value
            self._notify_subscribers('/channel/' + str(channel_number) + '/send/'
                                     + str(self._auxes.index(aux.stereo_partner) + 1) + '/pre_post')
        if channel.stereo_partner:
            # If stereo, also switch second channels send
            channel.stereo_partner.aux_sends[aux].pre_post = value
            self._notify_subscribers(
                '/channel/' + str(self._channels.index(channel.stereo_partner) +
                                  1) + '/send/' + str(aux_number) + '/pre_post')
            if aux.stereo_partner:
                # If stereo aux, also switch second auxs send
                channel.stereo_partner.aux_sends[aux.stereo_partner].pre_post = value
                self._notify_subscribers('/channel/' +
                                         str(self._channels.index(
                                             channel.stereo_partner) + 1) +
                                         '/send/' +
                                         str(self._auxes.index(
                                             aux.stereo_partner) + 1) +
                                         '/pre_post')

    def set_channel_name(self, channel_number, value, **kwargs):
        """Set the name of a channel

        value: a string"""
        channel = self._channels[channel_number - 1]
        channel.name = value.strip()
        self._notify_subscribers('/channel/' + str(channel_number) + '/name')

    def set_channel_name_char(self, channel_number, character_number, value,
                              **kwargs):
        """Set a character of the channels name

        value: the ASCII value of the character"""
        channel = self._channels[channel_number - 1]
        name_list = [character for character in channel.name]
        for i in range(character_number - len(name_list)):
            name_list.append(' ')
        name_list[character_number - 1] = chr(value)
        self.set_channel_name(channel_number, ''.join(name_list))

    def activate_aux(self, aux_number, value, **kwargs):
        """Activate or deactivate an aux"""
        logger.debug('Activate Aux {aux} to {value}'.format(aux=aux_number,
                                                            value=value))
        aux = self._auxes[aux_number - 1]
        # Cast value to bool
        value = bool(value)
        aux.active = value
        self._notify_subscribers('/aux/' + str(aux_number) + '/mute')
        if aux.stereo_partner:
            # If stereo, also activate second aux
            aux.stereo_partner.active = value
            self._notify_subscribers(
                '/aux/' + str(self._auxes.index(aux.stereo_partner) + 1) +
                '/mute')

    def set_aux_stereo(self, aux_number, value, **kwargs):
        """Switch a aux between stereo and mono"""
        # Cast value to bool
        value = bool(value)
        second_aux_number = aux_number + (1 if aux_number % 2 else -1)
        logger.debug('Set auxes {aux} and {aux_2} stereo {value}'
                     .format(aux=aux_number, aux_2=second_aux_number,
                             value=value))
        self._auxes[aux_number - 1].stereo_partner = \
            self._auxes[second_aux_number - 1] if value else None
        self._auxes[second_aux_number - 1].stereo_partner = \
            self._auxes[aux_number - 1] if value else None
        self._notify_subscribers('/aux/' + str(aux_number) + '/stereo')
        self._notify_subscribers('/aux/' + str(second_aux_number) + '/stereo')

    def fade_aux(self, aux_number, value, **kwargs):
        """Change the level of an aux"""
        logger.debug('Fade Aux {aux} to {value}'.format(aux=aux_number,
                                                        value=value))
        aux = self._auxes[aux_number - 1]
        aux.level = value
        self._notify_subscribers('/aux/' + str(aux_number) + '/level')
        if aux.stereo_partner:
            # If stereo, also fade second aux
            aux.stereo_partner.level = value
            self._notify_subscribers(
                '/aux/' + str(self._auxes.index(aux.stereo_partner) + 1) +
                '/level')

    def set_aux_name(self, aux_number, value, **kwargs):
        """Set the name of an aux

        value: a string"""
        aux = self._auxes[aux_number - 1]
        aux.name = value.strip()
        self._notify_subscribers('/aux/' + str(aux_number) + '/name')

    def set_aux_name_char(self, aux_number, character_number, value, **kwargs):
        """Set a character of the auxes name

        value: the ASCII value of the character"""
        aux = self._auxes[aux_number - 1]
        name_list = [character for character in aux.name]
        for i in range(character_number - len(name_list)):
            name_list.append(' ')
        name_list[character_number - 1] = chr(value)
        self.set_aux_name(aux_number, ''.join(name_list))

    def activate_bus(self, bus_number, value, **kwargs):
        """Activate or deactivate a bus"""
        logger.debug('Activate Bus {bus} to {value}'.format(bus=bus_number,
                                                            value=value))
        bus = self._buses[bus_number - 1]
        # Cast value to bool
        value = bool(value)
        bus.active = value
        self._notify_subscribers('/bus/' + str(bus_number) + '/mute')
        if bus.stereo_partner:
            # If stereo, also activate second bus
            bus.stereo_partner.active = value
            self._notify_subscribers(
                '/bus/' + str(self._buses.index(bus.stereo_partner) + 1) +
                '/mute')

    def set_bus_stereo(self, bus_number, value, **kwargs):
        """Switch a bus between stereo and mono"""
        # Cast value to bool
        value = bool(value)
        second_bus_number = bus_number + (1 if bus_number % 2 else -1)
        logger.debug('Set buses {bus} and {bus_2} stereo {value}'
                     .format(bus=bus_number, bus_2=second_bus_number,
                             value=value))
        self._buses[bus_number - 1].stereo_partner = \
            self._buses[second_bus_number - 1] if value else None
        self._buses[second_bus_number - 1].stereo_partner = \
            self._buses[bus_number - 1] if value else None
        self._notify_subscribers('/bus/' + str(bus_number) + '/stereo')
        self._notify_subscribers('/bus/' + str(second_bus_number) + '/stereo')

    def fade_bus(self, bus_number, value, **kwargs):
        """Change the level of a bus"""
        logger.debug('Fade Bus {bus} to {value}'.format(bus=bus_number,
                                                        value=value))
        bus = self._buses[bus_number - 1]
        bus.level = value
        self._notify_subscribers('/bus/' + str(bus_number) + '/level')
        if bus.stereo_partner:
            # If stereo, also fade second bus
            bus.stereo_partner.level = value
            self._notify_subscribers(
                '/bus/' + str(self._buses.index(bus.stereo_partner) + 1) +
                '/level')

    def set_bus_name(self, bus_number, value, **kwargs):
        """Set the name of a bus

        value: a string"""
        bus = self._buses[bus_number - 1]
        bus.name = value.strip()
        self._notify_subscribers('/bus/' + str(bus_number) + '/name')

    def set_bus_name_char(self, bus_number, character_number, value, **kwargs):
        """Set a character of the buses name

        value: the ASCII value of the character"""
        bus = self._buses[bus_number - 1]
        name_list = [character for character in bus.name]
        for i in range(character_number - len(name_list)):
            name_list.append(' ')
        name_list[character_number - 1] = chr(value)
        self.set_bus_name(bus_number, ''.join(name_list))

    def activate_main(self, value, **kwargs):
        """Activate or deactivate the main"""
        logger.debug('Activate Main to {value}'.format(value=value))
        # Cast value to bool
        value = bool(value)
        self._main.active = value
        self._notify_subscribers('/main/mute')

    def fade_main(self, value, **kwargs):
        """Change the level of the main"""
        logger.debug('Fade Main to {value}'.format(value=value))
        self._main.level = value
        self._notify_subscribers('/main/level')

    def is_channel_active(self, channel_number, **kwargs):
        """Get the active status of a channel"""
        return self._channels[channel_number - 1].active

    def is_channel_stereo(self, channel_number, **kwargs):
        """Get the stereo status of a channel"""
        return bool(self._channels[channel_number - 1].stereo_partner)

    def get_channel_level(self, channel_number, **kwargs):
        """Get the level of a channel"""
        return self._channels[channel_number - 1].level

    def get_channel_aux_level(self, channel_number, aux_number, **kwargs):
        """Get the send level of a channel to an aux"""
        aux = self._auxes[aux_number - 1]
        return self._channels[channel_number - 1].aux_sends[aux].level

    def get_pre_post_channel_aux(self, channel_number, aux_number, **kwargs):
        """Get pre/post of a channels send to an aux"""
        aux = self._auxes[aux_number - 1]
        return self._channels[channel_number - 1].aux_sends[aux].pre_post

    def get_channel_name(self, channel_number, **kwargs):
        """Get the name of a channel"""
        channel = self._channels[channel_number - 1]
        return channel.name if channel.name else 'Channel ' + str(channel_number)

    def is_aux_active(self, aux_number, **kwargs):
        """Get the active status of an aux"""
        return self._auxes[aux_number - 1].active

    def is_aux_stereo(self, aux_number, **kwargs):
        """Get the stereo status of a aux"""
        return bool(self._auxes[aux_number - 1].stereo_partner)

    def get_aux_level(self, aux_number, **kwargs):
        """Get the level of an aux"""
        return self._auxes[aux_number - 1].level

    def get_aux_name(self, aux_number, **kwargs):
        """Get the name of a aux"""
        aux = self._auxes[aux_number - 1]
        return aux.name if aux.name else 'Aux ' + str(aux_number)

    def is_bus_active(self, bus_number, **kwargs):
        """Get the active status of an aux"""
        return self._buses[bus_number - 1].active

    def is_bus_stereo(self, bus_number, **kwargs):
        """Get the stereo status of a bus"""
        return bool(self._buses[bus_number - 1].stereo_partner)

    def get_bus_level(self, bus_number, **kwargs):
        """Get the level of an aux"""
        return self._buses[bus_number - 1].level

    def get_bus_name(self, bus_number, **kwargs):
        """Get the name of a bus"""
        bus = self._buses[bus_number - 1]
        return bus.name if bus.name else 'Bus ' + str(bus_number)

    def is_main_active(self, **kwargs):
        """Get the active status of the main"""
        return self._main.active

    def get_main_level(self, **kwargs):
        """Get the level of the main"""
        return self._main.level

    def request_channel_data(self, channel_number):
        """Request all data belonging to a channel

        To be implemented by inheriting class
        """
        pass

    def request_main_data(self):
        """Request all data belonging to main

        To be implemented by inheriting class
        """
        pass

    def request_aux_data(self, aux_number):
        """Request all data belonging to a aux

        To be implemented by inheriting class
        """
        pass

    def request_bus_data(self, bus_number):
        """Request all data belonging to a bus

        To be implemented by inheriting class
        """
        pass

    def synchronize(self):
        """Synchronize the data of the mixer board"""
        logger.info('Synchronizing data from mixer')
        # Get channel data
        for i in range(self._channel_count):
            self.request_channel_data(i + 1)
        # Get aux data
        for i in range(self._aux_count):
            self.request_aux_data(i + 1)
        # Get bus data
        for i in range(self._bus_count):
            self.request_bus_data(i + 1)
        # Get main data
        self.request_main_data()

    def subscribe(self, subscriber):
        """Method to subscribe a subscriber for updates on value changes"""
        logging.debug('New subscriber to mixer {subscriber}'
                      .format(subscriber=subscriber))
        self._subscribers.append(subscriber)

    def unsubscribe(self, subscriber):
        """Method to unsubscribe a subscriber for no more updates"""
        logging.debug('Removing subscriber from mixer {subscriber}'
                      .format(subscriber=subscriber))
        self._subscribers.remove(subscriber)

    def _notify_subscribers(self, value_id):
        """Notify all subscribers that the model has been changed"""
        for subscriber in self._subscribers:
            subscriber.value_changed(value_id)

    def __str__(self):
        """For using the print function with the mixer"""
        string = self.type_string + '\n'
        string += 'Main: Active: {active}, Level: {level}\n' \
            .format(active=self._main.active, level=self._main.level)
        string += '--------------------------------\n'
        string += 'Auxs | Active | Level\n'
        string += '--------------------------------\n'
        for i, aux in zip(range(self._aux_count), self._auxes):
            string += '{aux_number:>4} | {active:>6} | {level:1.3f}\n' \
                .format(aux_number=i + 1, active=aux.active, level=aux.level)
        string += '--------------------------------\n'
        string += 'Channel | Active | Level | Sends\n'
        string += '--------------------------------\n'
        for i, channel in zip(range(self._channel_count), self._channels):
            string += '{channel_number:>7} | {active:>6} | {level:1.3f} | ' \
                .format(channel_number=i + 1, active=channel.active,
                        level=channel.level)
            for send in channel.aux_sends:
                string += '({pre_post:>5} {level:1.3f}), ' \
                    .format(pre_post=send.pre_post, level=send.level)
            string += '\n'
        return string

    def __repr__(self):
        return 'Mixer(\n' + self.__str__() + ')'


class Channel:
    """Class holding all data belonging to a channel"""

    def __init__(self, auxes):
        self.level = 0
        self.active = True
        self.aux_sends = {aux: Send(aux) for aux in auxes}
        self.name = ''
        self.stereo_partner = None


class Aux:
    """Class holding all data belonging to an aux out"""

    def __init__(self):
        self.name = ''
        self.level = 0
        self.active = True
        self.stereo_partner = None


class Bus:
    """Class holding information belonging to a bus"""

    def __init__(self):
        self.name = ''
        self.level = 0
        self.active = True
        self.stereo_partner = None


class Send:
    """Class holding all data belonging to an aux send"""

    def __init__(self, aux):
        self.target = aux
        self.pre_post = False
        self.level = 0


class MixerSubscriber:
    """'Interface' for subscribers to a mixer"""

    def __init__(self, mixer):
        self._mixer = mixer
        mixer.subscribe(self)

    def value_changed(self, value_id):
        """Method calld by the mixer the subscriber is subscribed to

        Needs to be implementd by the inheriting class
        """
        pass
