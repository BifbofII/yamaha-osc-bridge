"""Everything regarding the OSC server for the OSC bridge

Module: oscServer
Author: Christoph Jabs
Project: YamahaOSCBridge
"""

# External imports
import threading
import re
import time
import socketserver
import logging
from pythonosc.osc_server import ThreadingOSCUDPServer
from pythonosc.dispatcher import Dispatcher
from pythonosc.udp_client import SimpleUDPClient
from pythonosc import osc_packet
# Internal imports
from .mixers.basics import MixerSubscriber

logger = logging.getLogger(__name__)


class _UDPHandlerWithClientInfo(socketserver.BaseRequestHandler):
    """Handle UDP requests with passing of client info"""

    def handle(self):
        """
        This function calls the handlers registered to the dispatcher for
        every message it found in the packet.
        The process/thread granularity is thus the OSC packet, not the handler.
        If parameters were registered with the dispatcher, then the handlers are
        called this way:
            handler('/address that triggered the message',
                    registered_param_list, osc_msg_arg1, osc_msg_arg2, ...)
        if no parameters were registered, then it is just called like this:
            handler('/address that triggered the message',
                    osc_msg_arg1, osc_msg_arg2, osc_msg_param3, ...)

        Largely based on _call_handlers_for_packet from here:
        https://github.com/attwad/python-osc/blob/master/pythonosc/osc_server.py
        """
        # Get OSC messages from all bundles or standalone message.
        try:
            packet = osc_packet.OscPacket(self.request[0])
            for timed_msg in packet.messages:
                now = time.time()
                handlers = self.server.dispatcher.handlers_for_address(
                    timed_msg.message.address)
                if not handlers:
                    continue
                # If the message is to be handled later, then so be it.
                if timed_msg.time > now:
                    time.sleep(timed_msg.time - now)
                for handler in handlers:
                    if handler.args:
                        handler.callback(timed_msg.message.address, handler.args,
                                         *timed_msg.message, self.client_address)
                    else:
                        handler.callback(timed_msg.message.address,
                                         *timed_msg.message, self.client_address)
        except osc_packet.ParseError:
            pass


def get_used_numbers_from_match(match, standard_value=0):
    """Extracts the used numbers from a regex match and returns them, as a dict

    The numbers are:
        channel_number
        aux_number
        bus_number
    These are also the dict keys
    If the number is not found, the stadard value is set
    """
    number_names = ['channel_number', 'aux_number', 'bus_number']
    numbers = dict()
    for name in number_names:
        try:
            numbers[name] = int(match.group(name))
        except IndexError:
            numbers[name] = standard_value
    return numbers


class OSCBridgeServer(ThreadingOSCUDPServer, MixerSubscriber):
    """Class representing the OSC server for the bridge"""

    def __init__(self, server_ip, server_port, mixer):
        MixerSubscriber.__init__(self, mixer)
        self._mixer = mixer
        dispatcher = Dispatcher()
        dispatcher.map('/settings/*', self._dispatch_settings)
        dispatcher.map('/channel/*', self._dispatch_commands)
        dispatcher.map('/main/*', self._dispatch_commands)
        dispatcher.map('/aux/*', self._dispatch_commands)
        dispatcher.map('/bus/*', self._dispatch_commands)
        ThreadingOSCUDPServer.__init__(self, (server_ip, server_port),
                                       dispatcher)
        # Override RequestHandlerClass
        self.RequestHandlerClass = _UDPHandlerWithClientInfo
        self._server_thread = threading.Thread(target=self.serve_forever)
        self._clients = dict()
        self._command_map = {'/main/mute':
                             (self._mixer.activate_main,
                              self._mixer.is_main_active),
                             '/main/level':
                             (self._mixer.fade_main,
                              self._mixer.get_main_level),
                             '/channel/(?P<channel_number>[0-9]+)/mute':
                             (self._mixer.activate_channel,
                              self._mixer.is_channel_active),
                             '/channel/(?P<channel_number>[0-9]+)/name':
                             (self._mixer.set_channel_name,
                              self._mixer.get_channel_name),
                             '/channel/(?P<channel_number>[0-9]+)/level':
                             (self._mixer.fade_channel,
                              self._mixer.get_channel_level),
                             '/channel/(?P<channel_number>[0-9]+)/send/'
                             '(?P<aux_number>[0-9]+)/level':
                             (self._mixer.fade_channel_aux,
                              self._mixer.get_channel_aux_level),
                             '/channel/(?P<channel_number>[0-9]+)/send/'
                             '(?P<aux_number>[0-9]+)/pre_post':
                             (self._mixer.pre_post_channel_aux,
                              self._mixer.get_pre_post_channel_aux),
                             '/aux/(?P<aux_number>[0-9]+)/mute':
                             (self._mixer.activate_aux,
                              self._mixer.is_aux_active),
                             '/aux/(?P<aux_number>[0-9]+)/name':
                             (self._mixer.set_aux_name,
                              self._mixer.get_aux_name),
                             '/aux/(?P<aux_number>[0-9]+)/level':
                             (self._mixer.fade_aux,
                              self._mixer.get_aux_level),
                             '/bus/(?P<bus_number>[0-9]+)/mute':
                             (self._mixer.activate_bus,
                              self._mixer.is_bus_active),
                             '/bus/(?P<bus_number>[0-9]+)/name':
                             (self._mixer.set_bus_name,
                              self._mixer.get_bus_name),
                             '/bus/(?P<bus_number>[0-9]+)/level':
                             (self._mixer.fade_bus,
                              self._mixer.get_bus_level)}
        self._settings_map = {'/select_aux/(?P<aux_number>[0-9]+)':
                              OSCClient.select_aux,
                              '/select_bus/(?P<bus_number>[0-9]+)':
                              OSCClient.select_bus,
                              '/select_channel/(?P<channel_number>[0-9]+)':
                              OSCClient.select_channel}

    def value_changed(self, value_id):
        """Method called by the mixer if a value has been changed"""
        for key in self._command_map.keys():
            match = re.match(key, value_id)
            if match:
                numbers = get_used_numbers_from_match(match)
                value = self._command_map[key][1](
                    channel_number=numbers['channel_number'],
                    aux_number=numbers['aux_number'],
                    bus_number=numbers['bus_number'])
                self.send_to_all(value_id, value)

    def send_to_all(self, address, value):
        """Sends a message to all OSC clients"""
        logger.debug('Sending message at {address} with value {value}'
                     .format(address=address, value=value))
        for client in self._clients.values():
            client.send_message(address, value)

    def add_client(self, client_ip, port):
        """Add a client to the server to recieve updates"""
        logger.info('Adding and syncing client at {ip}:{port}'
                    .format(ip=client_ip, port=port))
        self._clients[(client_ip, port)] = OSCClient(client_ip, port, self)
        self._clients[(client_ip, port)].synchronize()

    def remove_client(self, client_ip, port):
        """Remove a client to recieve no more server updates"""
        logger.info('Removing client at {ip}:{port}'.format(ip=client_ip,
                                                            port=port))
        del self._clients[(client_ip, port)]

    def get_client(self, client_ip, port):
        """Returns the client with the given ip and port, if it's subscribed

        If the given client is not subscribed, the functions raises an IdexError
        """
        return self._clients[(client_ip, port)]

    def server_in_background(self):
        """Starts the server on a different thread"""
        self._server_thread.start()

    def _dispatch_commands(self, address, value, client_data):
        """Handle incomming OSC commands intended for the mixer

        address: the address of the incomming OSC command
        value: the value of the incomming OSC command
        """
        logger.debug('Recieved command {address} {value} form {ip}:{port}'
                     .format(address=address, value=value, ip=client_data[0],
                             port=client_data[1]))
        try:
            client = self.get_client(*client_data)
            # Replace variables in address
            address = re.sub('<channel_nr>', str(client._selected_channel_number),
                             address)
            address = re.sub('<aux_nr>', str(
                client._selected_aux_number), address)
        except KeyError:
            logger.info('Command from unsubscribed client {ip}:{port}'
                        .format(ip=client_data[0], port=client_data[1]))
        for key in self._command_map.keys():
            match = re.match(key, address)
            if match:
                numbers = get_used_numbers_from_match(match)
                self._command_map[key][0](value=value,
                                          channel_number=numbers['channel_number'],
                                          aux_number=numbers['aux_number'],
                                          bus_number=numbers['bus_number'])

    def _dispatch_settings(self, address, value, client_data):
        """Handle incomming OSC messages not intended for the mixer

        These are settings like the selected aux
        address: the address of the incomming OSC command
        value: the value of the incomming OSC command
        """
        logger.debug('Recieved setting {address} {value} from {ip}:{port}'
                     .format(address=address, value=value, ip=client_data[0],
                             port=client_data[1]))
        if address == '/settings/connect':
            self.add_client(*client_data)
            return
        if address == '/settings/resync':
            self._mixer.synchronize()
            return
        try:
            client = self.get_client(*client_data)
        except KeyError:
            logger.info('Settings from unsubscribed client {ip}:{port}'
                        .format(ip=client_data[0], port=client_data[1]))
        for key in self._settings_map.keys():
            match = re.match(key, address[9:])
            if match:
                numbers = get_used_numbers_from_match(match)
                self._settings_map[key](client,
                                        channel_number=numbers['channel_number'],
                                        aux_number=numbers['aux_number'],
                                        bus_number=numbers['bus_number'])


class OSCClient(SimpleUDPClient):
    """Class representing a client to the OSC server

    Hold values belonging to the client
    """
    _patterns = ['/channel/(?P<channel_number>[0-9]+)/mute',
                 '/channel/(?P<channel_number>[0-9]+)/name',
                 '/channel/(?P<channel_number>[0-9]+)/level',
                 '/channel/(?P<channel_number>[0-9]+)/send/'
                 '(?P<aux_number>[0-9]+)/level',
                 '/channel/(?P<channel_number>[0-9]+)/send/'
                 '(?P<aux_number>[0-9]+)/pre_post',
                 '/aux/(?P<aux_number>[0-9]+)/mute',
                 '/aux/(?P<aux_number>[0-9]+)/name',
                 '/aux/(?P<aux_number>[0-9]+)/level',
                 '/bus/(?P<bus_number>[0-9]+)/mute',
                 '/bus/(?P<bus_number>[0-9]+)/name',
                 '/bus/(?P<bus_number>[0-9]+)/level']
    # Time to wait after each OSC message
    time_per_message = 0.0005

    def __init__(self, ip, port, server):
        super().__init__(ip, port)
        self._ip = ip
        self._port = port
        self._selected_aux_number = 1
        self._selected_bus_number = 1
        self._selected_channel_number = 1
        self._server = server

    def select_aux(self, aux_number, **kwargs):
        """Select which aux to use"""
        if aux_number != self._selected_aux_number:
            # Deselect last button
            self.send_message('/settings/select_aux/' +
                              str(self._selected_aux_number), 0)
            self._selected_aux_number = int(aux_number)
            # Select new button
            self.send_message('/settings/select_aux/' + str(aux_number), 1)
            self._synchronize_aux_data(int(aux_number))

    def select_bus(self, bus_number, **kwargs):
        """Select which bus to use"""
        if bus_number != self._selected_bus_number:
            self.send_message('/settings/select_bus/' +
                              str(self._selected_bus_number), 0)
            self._selected_bus_number = int(bus_number)
            # Select new button
            self.send_message('/settings/select_bus/' + str(bus_number), 1)
            self._synchronize_bus_data(int(bus_number))

    def select_channel(self, channel_number, **kwargs):
        """Select which channel to use"""
        if channel_number != self._selected_channel_number:
            self.send_message('/settings/select_channel/' +
                              str(self._selected_channel_number), 0)
            self._selected_channel = int(channel_number)
            # Select new button
            self.send_message('/settings/select_channel/' + str(channel_number),
                              1)
            self._synchronize_channel_data(int(channel_number))

    def synchronize(self):
        """Synchronize the client with the server"""
        # Send channel data
        for i in range(self._server._mixer._channel_count):
            self._synchronize_channel_data(i + 1)
        # Send aux data
        for i in range(self._server._mixer._aux_count):
            self._synchronize_aux_data(i + 1)
        # Send bus data
        for i in range(self._server._mixer._bus_count):
            self._synchronize_bus_data(i + 1)
        # Send main data
        self._synchronize_main_data()
        # Send settings data
        self._synchronize_settings_data()

    def _synchronize_channel_data(self, channel_number):
        """Synchronize the data of one channel"""
        channel = self._server._mixer._channels[channel_number - 1]
        self.send_message('/channel/' + str(channel_number) + '/name',
                          channel.name if channel.name else 'Channel ' +
                          str(channel_number))
        self.send_message('/channel/' + str(channel_number) + '/level',
                          channel.level)
        self.send_message('/channel/' + str(channel_number) + '/mute',
                          channel.active)
        self.send_message('/channel/' + str(channel_number) + '/stereo',
                          bool(channel.stereo_partner))
        for i, aux in zip(range(self._server._mixer._aux_count),
                           self._server._mixer._auxes):
            self.send_message('/channel/' + str(channel_number) + '/send/' +
                              str(i + 1) + '/level',
                              channel.aux_sends[aux].level)
            self.send_message('/channel/' + str(channel_number) + '/send/' +
                              str(i + 1) + '/pre_post',
                              channel.aux_sends[aux].pre_post)

    def _synchronize_aux_data(self, aux_number):
        """Synchronize the data of one aux"""
        aux = self._server._mixer._auxes[aux_number - 1]
        self.send_message('/aux/' + str(aux_number) + '/name',
                          aux.name if aux.name else 'Aux ' + str(aux_number))
        self.send_message('/aux/' + str(aux_number) + '/level', aux.level)
        self.send_message('/aux/' + str(aux_number) + '/mute', aux.active)
        self.send_message('/aux/' + str(aux_number) + '/stereo',
                          bool(aux.stereo_partner))
        for i, channel in zip(range(self._server._mixer._channel_count),
                              self._server._mixer._channels):
            send = channel.aux_sends[aux]
            self.send_message('/channel/' + str(i + 1) + '/send/' +
                              str(aux_number) + '/level', send.level)
            self.send_message('/channel/' + str(i + 1) + '/send/' +
                              str(aux_number) + '/pre_post', send.pre_post)

    def _synchronize_bus_data(self, bus_number):
        """Synchronize the data of one bus"""
        bus = self._server._mixer._buses[bus_number - 1]
        self.send_message('/bus/' + str(bus_number) + '/level', bus.level)
        self.send_message('/bus/' + str(bus_number) + '/mute', bus.active)
        self.send_message('/bus/' + str(bus_number) + '/stereo',
                          bool(bus.stereo_partner))

    def _synchronize_main_data(self):
        """Synchronize the data of the main"""
        self.send_message('/main/level', self._server._mixer._main.level)
        self.send_message('/main/mute', self._server._mixer._main.active)

    def _synchronize_settings_data(self):
        """Synchronize the settings with the client"""
        # Send selected channel data
        for channel_number in range(self._server._mixer._channel_count):
            self.send_message('/settings/select_channel/' +
                              str(channel_number + 1),
                              channel_number + 1 == self._selected_channel_number)
        # Send selected aux data
        for aux_number in range(self._server._mixer._aux_count):
            self.send_message('/settings/select_aux/' + str(aux_number + 1),
                              aux_number + 1 == self._selected_aux_number)
        # Send selected bus data
        for bus_number in range(self._server._mixer._bus_count):
            self.send_message('/settings/select_bus/' + str(bus_number + 1),
                              bus_number + 1 == self._selected_bus_number)

    def send_message(self, address, value):
        """Adjust a few things from standard send_message method

        Add slight delay to not overflow buffer
        Automatically send messages for selected channel/aux
        """
        super().send_message(address, value)
        # Check if message is for selected channel/aux
        for pattern in self._patterns:
            match = re.match(pattern, address)
            if match:
                numbers = get_used_numbers_from_match(match)
                channel_tokens = [str(numbers['channel_number'])]
                aux_tokens = [str(numbers['aux_number'])]
                bus_tokens = [str(numbers['bus_number'])]
                if numbers['channel_number'] == self._selected_channel_number:
                    channel_tokens.append('<channel_nr>')
                if numbers['aux_number'] == self._selected_aux_number:
                    aux_tokens.append('<aux_nr>')
                if numbers['bus_number'] == self._selected_bus_number:
                    aux_tokens.append('<bus_nr>')
                if len(channel_tokens) > 1 or len(aux_tokens) > 1:
                    for channel_token in channel_tokens:
                        for aux_token in aux_tokens:
                            for bus_token in bus_tokens:
                                new_address = pattern
                                new_address = re.sub(
                                    re.escape('(?P<channel_number>[0-9]+)'),
                                    channel_token, new_address)
                                new_address = re.sub(
                                    re.escape('(?P<aux_number>[0-9]+)'),
                                    aux_token, new_address)
                                new_address = re.sub(
                                    re.escape('(?P<bus_number>[0-9]+)'),
                                    bus_token, new_address)
                                super().send_message(new_address, value)
                                time.sleep(self.time_per_message)
        # Assume 50us should be enough
        time.sleep(self.time_per_message)
