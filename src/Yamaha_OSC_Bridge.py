#!/bin/env python3

"""The main script of the project

Module: yamahaOSCBridge
Author: Christoph Jabs
Project: YamahaOSCBridge
"""

# External imports
import mido
import sys
import socket
import argparse
import logging
# Internal imports
import yamahaOSCBridge
from yamahaOSCBridge.mixers import yamaha
from yamahaOSCBridge import oscServer

# Get logger that holds all child loggers from inside modules
logger = logging.getLogger('yamahaOSCBridge')
log_levels = [logging.WARNING, logging.INFO, logging.DEBUG]


def interactive_server_setup():
    """Function for interactively setting up a OSC bridge"""
    logger.warning('Searching for connected mixers...')
    connected_mixers = auto_detect_mixers()
    if len(connected_mixers) == 0:
        # No mixer found
        logger.error('No mixer connected, quitting')
        wait_for_user(True)
    elif len(connected_mixers) == 1:
        # One mixer found, autoconnect
        found_mixer = connected_mixers[0]
        logger.warning('Found mixer of type {type} at {port_name} with id {id_nr}, '
                       'connecting to it'.format(type=found_mixer[0].type_string,
                                                 port_name=found_mixer[1],
                                                 id_nr=found_mixer[2]))
        mixer = found_mixer[0](found_mixer[1][0], found_mixer[1][1],
                               found_mixer[2])
    else:
        # Multiple mixers found, let user decide
        print('Found multiple mixers:')
        for i, mixer in zip(range(len(connected_mixers)), connected_mixers):
            print("{index:2}: {type} at '{port}' with id {id_nr}"
                  .format(index=i + 1, type=mixer[0].type_string, port=mixer[1],
                          id_nr=mixer[2]))
        mixer_number = \
            int(input('Enter the number of the mixer you want to connect: '))
        selected_mixer = connected_mixers[mixer_number - 1]
        logger.warning('Connecting mixer of type {type} at {port_name} with id {id_nr}'
                       .format(type=selected_mixer[0].type_string,
                               port_name=selected_mixer[1],
                               id_nr=selected_mixer[2]))
        mixer = selected_mixer[0](selected_mixer[1][0], selected_mixer[1][1],
                                  selected_mixer[2])
    # Ask user for IP (autodetect default)
    ip_address = socket.gethostbyname(socket.gethostname())
    user_ip = input('What IP do you want to use for the OSC server '
                    '({ip}): '.format(ip=ip_address))
    if user_ip != '':
        ip_address = user_ip
    # Ask user for port (8000 default)
    port = 8000
    user_port = input('What port do you want to use for the OSC server '
                      '({port}): '.format(port=port))
    if user_port != '':
        port = int(user_port)
    # Start server
    server = oscServer.OSCBridgeServer(ip_address, port, mixer)
    logger.warning('Starting server at {ip}:{port}'.format(
        ip=ip_address, port=port))
    server.serve_forever()


def auto_detect_mixers():
    """Detects connected mixers and returns the first one

    Return value contains (<class of mixer type>, <midi_port_name>, <mixer_id>)
    """
    midi_port_names = mido.get_input_names()
    connected_mixers = []
    output_ports = []
    for midi_port_name in midi_port_names:
        with mido.open_input(midi_port_name) as midi_port:
            found_mixers = yamaha.check_for_connected_mixers(midi_port)
            for mixer in found_mixers:
                output_ports.append(yamaha.find_output_port(midi_port, mixer[0],
                                                            mixer[1]))
            connected_mixers += [(mixer[0], (midi_port_name, output_port_name),
                                  mixer[1])
                                 for mixer, output_port_name in
                                 zip(found_mixers, output_ports)]
    return connected_mixers


def wait_for_user(exit=False):
    """Wait until the user acknowledges with the Enter key"""
    input("Press Enter to continue...")
    if exit:
        sys.exit(-1)


if __name__ == '__main__':
    # Set up cli argparser
    parser = argparse.ArgumentParser(
        description='Start a OSC bridge for a mixer board')
    parser.add_argument('-a', '--server-ip', default='127.0.0.1',
                        help='The IP to start the OSC server on')
    parser.add_argument('-p', '--server-port', type=int, default=8000,
                        help='The port to start the OSC server on')
    parser.add_argument('-s', '--slow-osc', const=0.001, default=0.0005,
                        action='store_const', dest='msg_time',
                        help='If sets, increases the time between OSC '
                        'messages, for slow networks')
    parser.add_argument('-i', '--interactive', action='store_true',
                        help="If set, run's an interactive script to setup ' \
                        'the server. This is the default")
    parser.add_argument('-M', '--auto-detect-mixer', action='store_true',
                        help='If set, autodetects connected mixers and uses '
                        'the first found one ore the one with the given ID')
    parser.add_argument('-A', '--auto-detect-ip', action='store_true',
                        help='If set, autodetects the local IP of the machine '
                        'and uses it as the server IP')
    parser.add_argument('-I', '--midi-input-port',
                        help='The name of the MIDI input port that should be used')
    parser.add_argument('-O', '--midi-output-port',
                        help='The name of the MIDI output port that should be used. '
                        'This can only be specified if a input port name is specified')
    parser.add_argument('-m', '--mixer-id', type=int,
                        help='Manually give the id value of the mixer')
    parser.add_argument('-v', '--verbose', action='count', dest='log_level',
                        default=0, help='Output more information to the '
                        'commandline, for debugging purposes')
    parser.add_argument('-l', '--log-file', help='The path to a file where the '
                        'log should be written')
    cli_args = parser.parse_args()
    # Set up logging
    logger.setLevel(logging.DEBUG)
    logger.propagate = False
    cli_logging_handler = logging.StreamHandler(sys.stdout)
    cli_logging_handler.setLevel(log_levels[cli_args.log_level])
    cli_logging_handler.setFormatter(
        logging.Formatter('%(levelname)s: %(message)s'))
    logger.addHandler(cli_logging_handler)
    if cli_args.log_file:
        log_file_handler = logging.FileHandler(cli_args.log_file)
        log_file_handler.setLevel(logging.DEBUG)
        log_file_handler.setFormatter(
            logging.Formatter('%(levelname)s: %(message)s'))
        logger.addHandler(log_file_handler)
    # Handle cli arguments
    if cli_args.interactive or len(sys.argv) == 1:
        logger.warning('Interactive option set, omitting other arguments')
        interactive_server_setup()
    else:
        mixer = None
        if cli_args.auto_detect_mixer:
            # Auto detect mixer
            logger.warning('Auto detecting mixers')
            mixers_data = auto_detect_mixers()
            if len(mixers_data) != 0:
                if not cli_args.mixer_id:
                    logger.warning('Connecting mixer of type {type} at {port_name} '
                                   'with id {id_nr}'
                                   .format(type=mixers_data[0][0].type_string,
                                           port_name=mixers_data[0][1],
                                           id_nr=mixers_data[0][2]))
                    mixer = mixers_data[0][0](mixers_data[0][1][0],
                                              mixers_data[0][1][1],
                                              mixers_data[0][2])
                else:
                    # Autodetect mixer, but specified ID
                    for mixer_data in mixers_data:
                        if mixer_data[2] == cli_args.mixer_id:
                            logger.warning('Connecting mixer of type {type} at '
                                           '{port_name} with id {id_nr}'
                                           .format(type=mixer_data[0].type_string,
                                                   port_name=mixer_data[1],
                                                   id_nr=mixer_data[2]))
                            mixer = mixer_data[0](mixer_data[1][0],
                                                  mixer_data[1][1],
                                                  mixer_data[2])
                    if not mixer:
                        logger.error('The mixer with the ID {id_number} could not '
                                     'be detected'.format(id_number=cli_args.mixer_id))
                        wait_for_user(True)
            else:
                logger.error('No mixer detected')
                wait_for_user(True)
        elif cli_args.midi_input_port:
            # MIDI input port specified
            with mido.open_input(cli_args.midi_input_port) as midi_input_port:
                found_mixers = yamaha.check_for_connected_mixers(
                    midi_input_port)
                if not cli_args.midi_output_port:
                    if not cli_args.mixer_id:
                        # No ID specified
                        # Determine output port of first found mixer
                        cli_args.midi_output_port = \
                            yamaha.find_output_port(midi_input_port,
                                                    found_mixers[0][0],
                                                    found_mixers[0][1])
                    else:
                        # Specified ID
                        for found_mixer in found_mixers:
                            if found_mixer[1] == cli_args.mixer_id:
                                yamaha.find_output_port(midi_input_port,
                                                        found_mixer[0],
                                                        found_mixer[1])
            if not cli_args.mixer_id:
                # No ID specified
                if len(found_mixers) == 0:
                    logger.warning('No mixer on given MIDI port')
                    wait_for_user(True)
                else:
                    logger.warning('Connecting mixer of type {type} at {port_name} '
                                   'with id {id_nr}'
                                   .format(type=found_mixers[0][0].type_string,
                                           port_name=(cli_args.midi_input_port,
                                                      cli_args.midi_output_port),
                                           id_nr=found_mixers[0][1]))
                    mixer = found_mixers[0][0](cli_args.midi_input_port,
                                               cli_args.midi_output_port,
                                               found_mixers[0][1])
            else:
                # Specified ID
                for found_mixer in found_mixers:
                    if found_mixer[1] == cli_args.mixer_id:
                        logger.warning('Connecting mixer of type {type} at {port_name} '
                                       'with id {id_nr}'
                                       .format(type=found_mixer[0].type_string,
                                               port_name=(cli_args.midi_input_port,
                                                          cli_args.midi_output_port),
                                               id_nr=found_mixer[1]))
                        mixer = found_mixer[0](clie_args.midi_input_port,
                                               cli_args.midi_output_port,
                                               found_mixer[1])
                if not mixer:
                    logger.error('The mixer with the ID {id_number} could not be '
                                 'found on the MIDI port {port}'
                                 .format(id_number=cli_args.mixer_id,
                                         port=cli_args.midi_port))
                    wait_for_user(True)
        else:
            # No MIDI port and no auto detect
            logger.error(
                'No MIDI port specified and auto detect, not set... Aborting')
            wait_for_user(True)
        # Setup OSC server
        if cli_args.auto_detect_ip:
            logger.warning('Autodetecting IP...')
            ip_address = socket.gethostbyname(socket.gethostname())
        else:
            ip_address = cli_args.server_ip
        port = cli_args.server_port
        logger.warning('Starting server at {ip}:{port}'.format(ip=ip_address,
                                                               port=port))
        server = oscServer.OSCBridgeServer(ip_address, port, mixer)
        oscServer.OSCClient.time_per_message = cli_args.msg_time
        server.serve_forever()
