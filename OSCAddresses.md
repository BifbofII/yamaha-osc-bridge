# Yamaha OSC Bridge --- OSC Address System

This document gives an overview over the OSC address system used by Yamaha OSC Bridge. It can be used to create custom layouts for OSC apps.

Here you can see all addresses understood by the server:

```
├── channel
│   └── <channel_nr>
│       ├── mute
│       ├── level
│       ├── name
│       └── send
│           └── <aux_nr>
│               ├── pre_post
│               └── level
├── aux
│   └── <aux_nr>
│       ├── mute
│       ├── level
│       └── name
├── bus
│   └── <bus_nr>
│       ├── mute
│       └── level
├── main
│   ├── mute
│   └── level
└── settings
    ├── connect
    ├── resync
    ├── select_channel
    │   └── <channel_nr>
    ├── select_aux
    │   └── <aux_nr>
    └── select_bus
        └── <bus_nr>
```

The tokens in `<>` can be either the token as written here or a number upwards from 1. If one of the tokens is found in the address, then the server replaces it with the selected number recieved via a settings message. The only messages you cant use the tokens in are the settings messages, there you need to use a number.

## Data Types

|Address ending |Data type  |
|---------------|-----------|
|mute           |boolean    |
|level          |float 0..1 |
|name           |string     |
|pre_post       |boolean    |

## Example Messages

- Fade channel 1 to 50%:
  `/channel/1/level 0.5`
- Switch aux 2 off:
  `/aux/2/mute False`
- Fade channel 15 to 30% on the selected aux:
  `/channel/15/send/<aux_nr>/level 0.3`
