# Yamaha OSC Bridge

Yamaha OSC Bridge is a small tool that let's you remote control a mixer board (only a Yamaha 01v96 for now) from a wirelessly connected device via the OSC protocol. It basically is a OSC server that recieves commands from your device and translates them to MIDI messages for the mixer board. The tool is written in python 3 but there are packaged versions for linux and windows, both 64 bit that do not have any dependencies. These packaged versions can be downloaded here:

- [Windows Cli Version](https://gitlab.com/BifbofII/yamaha-osc-bridge/raw/master/dist/Yamaha_OSC_Bridge_Windows_Cli.exe)
- [Linux Cli version](https://gitlab.com/BifbofII/yamaha-osc-bridge/raw/master/dist/Yamaha_OSC_Bridge_Linux_Cli)

If you want to run the source code directly in python, without a packaged version, you can find the dependencies [here](#dependencies).

## Usage

### Server Side

The mixer board must be connected to the PC you are running Yamaha OSC Bridge on via MIDI. The easiest way is using the USB MIDI interface built in to the Yamaha 01v96. The tool uses the same interface as the Studio Manager, so make sure that you configure the MIDI port to be used for the Studio Manager to be the one connected to your PC. You can specify the ID that yout configured the mixer with, but in most cases the tool should be able to auto detect the mixer.

To start the tool, just run the executable or the main python script (`src/Yamaha_OSC_Bridge.py`) without any arguments.

This will run a interactive setup of the tool. Another easy to use configuration can be run with the flags `-MA`. This will automatically detect the IP and the connected mixer and then start the server. If you want to change the IP or you have multiple mixers connected and want to choose which one to use, you can also configure the tool through different commandline switches (see the output of `<executable> -h` for more help).

### Client Side

There are different apps that support the OSC protocol that can be used on android and iOS. One app that can be used on both, android and iOS is [TouchOSC](https://hexler.net/software/touchosc). For TouchOSC, there are layouts that can be used in the `touchOSCLayouts` folder ([Here](https://hexler.net/docs/touchosc-configuration-layout-transfer-wifi) you can see how you can transfer the layouts to your device). Layouts for different apps will be gladly integrated in the project if provided through a pull request. If you want to create your own layouts, the address system can be found in the `OSCAddresses.md` file.

Connect your app to the running server (given the IP and port) and push the 'Connect' button. Now you should recieve updates from the server. Tho resynchronize data from the server, you can push the connect button again, to resynchronize the server with the mixer, you can push the 'Re-Synchronize' button.

## <a name="dependencies"/>Dependencies

- Python 3
- Python modules
  - mido
  - python-rtmidi
  - python-osc

The python modules can all be installed from the [PyPI](https://pypi.org/).
