# -*- mode: python -*-

block_cipher = None


a = Analysis(['../src/Yamaha_OSC_Bridge.py'],
             pathex=['/home/christoph/Git/YamahaOscBridge'],
             binaries=[],
             datas=[],
             hiddenimports=['mido.backends.rtmidi'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='Yamaha_OSC_Bridge_Linux_Cli',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
